Drools isn't supported in Intellij Community Edition (it requires Professional edition): https://stackoverflow.com/a/39574491
We can use alternative IDE's that support it such as Eclipse. I used Red Hat Developer studio as JBoss recommended, it has a lot of components we need built in apart from Drools itself

# How to import (into Red Hat Developer Studio)

1. If you haven't setup Red Hat Developer Studio, follow these steps: https://syn-confluence.tms-orbcomm.com:8800/display/AL/Drools#Drools-SettingupDrools
2. Clone this repo into your projects folder
3. Import this repo into your IDE. Click File -> Import -> General / Existing Projects Into Workspace
   Browse to the repo root directory and ensure this project is checked in the Projects window. Click Finish.
4. If the org.kie.api imports are failing to be recognised, follow step 5 of this guide: https://wwu-pi.github.io/tutorials/lectures/lsp/030_install_drools.html.
   Afterwards, right click the project in project explorer and select Properties. Click Drools on the left and ensure 'Enable Project Specific Settings' is checked.
   Click Apply and Close.
   
# How to run

1. Click 'Servers' from the tabs along the bottom pane. Right click 'Red Hat JBoss EAP 7.X [Stopped]' and click Start
2. Right-click the class in the project/package explorer (e.g. DroolsTest.java) and select Debug as...->Drools Application
