package com.alert;

import org.joda.time.DateTime;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.alert.objects.AlertState;
import com.alert.objects.AlertState.Status;
import com.alert.objects.DoorStatusRecord;

public class MainApplication {

	//Knowledge base
	//This contains all the .drl rules. Data can be inserted into this object with .insert() and then evaluated with .fireAllRules()
	private KieSession kSession;
	
	public MainApplication() {
        // load up the knowledge base
        KieServices ks = KieServices.Factory.get();
	    KieContainer kContainer = ks.getKieClasspathContainer();
    	kSession = kContainer.newKieSession("ksession-rules");
	}
	
	public void run() {
		
		//Create db data
		AlertState state = new AlertState();
		state.setStatus(Status.GREEN);
		
		DoorStatusRecord record = new DoorStatusRecord(DateTime.now().minusMinutes(1), true, false);
		
		//Insert db data into the Knowledge base
		kSession.insert(state);
		kSession.insert(record);
				
		kSession.fireAllRules();

		if(state.getStatus() == AlertState.Status.RED) {
			System.out.println("Test was successful");
		}else {
			throw new RuntimeException("Something went wrong");
		}
	}
}
