package com.alert.objects;

import org.joda.time.DateTime;

public class DoorStatusRecord {

	public DateTime timestamp;
	public Boolean door1Open;
	public Boolean door2Open;
	
	public DoorStatusRecord(DateTime timestamp, Boolean door1Open, Boolean door2Open) {
		super();
		this.timestamp = timestamp;
		this.door1Open = door1Open;
		this.door2Open = door2Open;
	}
	
	public DateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(DateTime timestamp) {
		this.timestamp = timestamp;
	}
	public Boolean isDoor1Open() {
		return door1Open;
	}
	public void setDoor1Open(Boolean door1Open) {
		this.door1Open = door1Open;
	}
	public Boolean isDoor2Open() {
		return door2Open;
	}
	public void setDoor2Open(Boolean door2Open) {
		this.door2Open = door2Open;
	}
	
}
