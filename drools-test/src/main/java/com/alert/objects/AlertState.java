package com.alert.objects;

public class AlertState {

	public enum Status {
		GREEN,
		AMBER,
		RED
	}
	
    private Status status;
    
    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
}
